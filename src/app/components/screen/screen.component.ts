import { Component, OnInit } from '@angular/core';
import { ScreenService } from 'src/app/services/screen.service';

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.css']
})
export class ScreenComponent implements OnInit {

  public screenData: any;
  public selectedStepIndex: number;

  public saveBtn: string;
  public saveBtnDisabled: boolean;

  public addStep: boolean = false;

  constructor(private screenService: ScreenService) {

    this.screenService.GetInitObject().subscribe(data => {
      console.log(data);
      this.screenData = data;
      Object.assign(this.screenData.step, this.screenData.steps[0]);
      this.selectedStepIndex = 0;
    }, error => {

    });

  }

  ngOnInit() {
    this.saveBtn = "Save";
    this.saveBtnDisabled = false;
  }

  GetStepItems(step, stepIndex) {
    Object.assign(this.screenData.step, step);
    this.selectedStepIndex = stepIndex;
    this.addStep = false;
  }

  SaveItem(isValid) {

    if (isValid) {
      this.saveBtn = "Saveing...";
      this.saveBtnDisabled = true;

      this.screenService.AddItem(this.screenData).subscribe(data => {
        this.screenData = data;
        console.log(this.screenData);

        this.saveBtn = "Save";
        this.saveBtnDisabled = false;
      }, error => {
        this.saveBtn = "Save";
        this.saveBtnDisabled = false;
      });
    }
  }

  SaveStep(isValid) {
    if (isValid) {
      this.saveBtn = "Saveing...";
      this.saveBtnDisabled = true;

      this.screenService.AddStep(this.screenData).subscribe(data => {
        this.screenData = data;

        this.saveBtn = "Save";
        this.saveBtnDisabled = false;
      }, error => {

        this.saveBtn = "Save";
        this.saveBtnDisabled = false;
      })
    }
  }

  ClearStepForm() {
    this.screenData.step.stepId = 0;
    this.screenData.step.stepName = "";
    this.screenData.step.stepDesc = "";
    this.screenData.step.items = [];

    this.addStep = true;
  }

  ClearItemForm() {
    this.screenData.item.itemDesc = "";
    this.screenData.item.itemId = 0;
    this.screenData.item.itemName = "";
    this.screenData.item.itemTitle = "";
    this.screenData.item.stepId = 0;
  }

  SelectItem(item) {
    Object.assign(this.screenData.item, item);
  }

  DeleteStep(step) {
    if (window.confirm("Do you want to delete " + step.stepName + " ?")) {
      this.screenService.RemoveStep(step).subscribe(data => {
        this.screenData = data;
        console.log(this.screenData);
      }, error => {

      })
    }
  }

  DeleteItem(item) {
    if (window.confirm("Do you want to delete " + item.itemName + " ?")) {
      this.screenService.RemoveItem(item).subscribe(data => {
        this.screenData = data;
        console.log(this.screenData);
      }, error => {

      })
    }
  }


  Previous() {
    if (this.selectedStepIndex - 1 < 0) {
      Object.assign(this.screenData.step, this.screenData.steps[this.screenData.steps.length - 1]);
      this.selectedStepIndex = this.screenData.steps.length - 1;
    }

    else {
      Object.assign(this.screenData.step, this.screenData.steps[this.selectedStepIndex - 1]);
      this.selectedStepIndex = this.selectedStepIndex - 1;
    }
  }

  Next() {
    if ((this.selectedStepIndex + 1) >= this.screenData.steps.length) {
      Object.assign(this.screenData.step, this.screenData.steps[0]);
      this.selectedStepIndex = 0;
    }

    else {
      Object.assign(this.screenData.step, this.screenData.steps[this.selectedStepIndex + 1]);
      this.selectedStepIndex = this.selectedStepIndex + 1;
    }
  }
}
