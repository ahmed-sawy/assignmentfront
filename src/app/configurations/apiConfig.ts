import { Injectable } from '@angular/core';


@Injectable()
export class apiConfig {
    baseURL: string = "http://localhost:49883";
    baseApiUrl: string = this.baseURL + "/api/";
}