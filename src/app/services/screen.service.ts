import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiConfig } from 'src/app/configurations/apiConfig';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScreenService {

  private GetStepsWithItemsURL: string;
  private AddNewStepURL: string;
  private AddNewItemAtSelectedStepURL: string;
  private RemoveStepURL: string;
  private RemoveItemURL: string;

  constructor(private http: HttpClient, private apiConfig: apiConfig) {

    this.GetStepsWithItemsURL = this.apiConfig.baseApiUrl + "Screen/GetInitObject";
    this.AddNewStepURL = this.apiConfig.baseApiUrl + "Screen/AddStep";
    this.AddNewItemAtSelectedStepURL = this.apiConfig.baseApiUrl + "Screen/AddItem";
    this.RemoveStepURL = this.apiConfig.baseApiUrl + "Screen/RemoveStep";
    this.RemoveItemURL = this.apiConfig.baseApiUrl + "Screen/RemoveItem";
  }


  public GetInitObject(): Observable<any> {
    return this.http.get<any>(this.GetStepsWithItemsURL);
  }

  public AddItem(screenData: any): Observable<any> {
    return this.http.post<any>(this.AddNewItemAtSelectedStepURL, screenData);
  }

  public AddStep(screenData: any): Observable<any> {
    return this.http.post<any>(this.AddNewStepURL, screenData);
  }

  public RemoveStep(step: any): Observable<any> {
    return this.http.post<any>(this.RemoveStepURL, step);
  }

  public RemoveItem(item: any): Observable<any> {
    return this.http.post<any>(this.RemoveItemURL, item);
  }
}
